﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        float hspeed = (horizontal * 10);
        float vspeed = (vertical * 10);
        GetComponent<Rigidbody>().AddForce(new Vector3(hspeed, 0, vspeed));
	}
}
